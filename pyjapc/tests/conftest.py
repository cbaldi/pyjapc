import cmmnbuild_dep_manager
import pytest

import jpype as jp

import pyjapc


@pytest.fixture
def enumtype_byte(jvm):
    """
    An EnumType of bitsize BYTE, which is also good for EnumItemSet testing.

    """
    EnumerationRegistry = jp.JClass("cern.japc.value.spi.value.EnumerationRegistry")
    EnumItemData = jp.JClass("cern.japc.value.spi.value.EnumItemData")
    EnumTypeBitSize = jp.JClass("cern.japc.value.EnumTypeBitSize")
    HashSet = jp.JClass("java.util.HashSet")

    item_set = HashSet()
    item_set.add(EnumItemData(1, "Item 1"))
    item_set.add(EnumItemData(2, "Item 2"))
    item_set.add(EnumItemData(4, "Item 3"))
    item_set.add(EnumItemData(8, "Item 4"))
    enum_type = EnumerationRegistry.registerEnumType("TestEnum", EnumTypeBitSize.BYTE, item_set)
    yield enum_type
    # We can't clear specific EnumTypes, so for now, just clear the whole lot.
    EnumerationRegistry.clearAll()


@pytest.fixture
def std_meaning_enum_descriptor(jvm):
    EnumerationRegistry = jp.JClass("cern.japc.value.spi.value.EnumerationRegistry")
    EnumItemData = jp.JClass("cern.japc.value.spi.value.EnumItemData")
    EnumTypeBitSize = jp.JClass("cern.japc.value.EnumTypeBitSize")
    HashSet = jp.JClass("java.util.HashSet")
    SimpleValueStandardMeaning = jp.JClass("cern.japc.value.SimpleValueStandardMeaning")

    item_set = HashSet()
    item_set.add(EnumItemData(42, "ON", SimpleValueStandardMeaning.ON, False))
    item_set.add(EnumItemData(-5, "OFF", SimpleValueStandardMeaning.OFF, True))
    enum_type = EnumerationRegistry.registerEnumType(
        "TestEnum", EnumTypeBitSize.BYTE, item_set)

    SimpleDescriptor = jp.JClass(
        "cern.japc.core.spi.value.SimpleDescriptorImpl")
    value_descriptor = SimpleDescriptor(
        jp.JClass("cern.japc.value.ValueType").ENUM, enum_type)

    yield value_descriptor
    # We can't clear specific EnumTypes, so for now, just clear the whole lot.
    EnumerationRegistry.clearAll()


@pytest.fixture(scope="module")
def jvm():
    mgr = cmmnbuild_dep_manager.Manager('pyjapc')
    mgr.start_jpype_jvm()
    return mgr


@pytest.fixture(scope="module")
def japc_instance(jvm):
    # The PyJapc instance itself is a singleton for the lifetime of the tests.
    return pyjapc.PyJapc(incaAcceleratorName=None)


@pytest.fixture
def japc(japc_instance):
    # Each PyJapc presented to each of the tests is the same instance, but has
    # been cleared back to its original state.
    yield japc_instance
    japc_instance.clearSubscriptions()


@pytest.fixture
def japc_mock(jvm):
    """
    Provide a JAPC mock instance that has some interesting capabilities:

        acqVal = cern.japc.ext.mockito.JapcMock.acqVal
        mockAllServices = cern.japc.ext.mockito.JapcMock.mockAllServices
        mockParameter = cern.japc.ext.mockito.JapcMock.mockParameter
        mpv = cern.japc.ext.mockito.JapcMock.mpv
        pe = cern.japc.ext.mockito.JapcMock.pe
        resetJapcMock = cern.japc.ext.mockito.JapcMock.resetJapcMock
        resetToDefault = cern.japc.ext.mockito.JapcMock.resetToDefault
        sel = cern.japc.ext.mockito.JapcMock.sel
        whenGetValueThen = cern.japc.ext.mockito.JapcMock.whenGetValueThen
        setGlobalAnswer = cern.japc.ext.mockito.JapcMock.setGlobalAnswer
        spv = cern.japc.ext.mockito.JapcMock.spv
        verify = org.mockito.Mockito.verify

    """
    mock = jp.JClass("cern.japc.ext.mockito.JapcMock")
    mock.mockAllServices()
    yield mock
    mock.resetToDefault()
