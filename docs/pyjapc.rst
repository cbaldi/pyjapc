PyJapc API
==========

pyjapc.PyJapc class
-------------------

.. autoclass:: pyjapc.PyJapc
    :members:
    :undoc-members:


pyjapc.rbac_dialog module
-------------------------

.. automodule:: pyjapc.rbac_dialog
    :members:
    :undoc-members:
